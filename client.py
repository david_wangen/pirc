import pirc
import interface
import curses
import sys

# Host and port
HOST = "irc.patwic.com"
PORT = 6667

# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)

# User data
NICK = "ducky"
REALNAME = "David Wangen"

# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# Join channel
irc.send("JOIN " + CHAN)


def main(stdscreen):

    ui = interface.TextUI(stdscreen)

    while True:
        msg = irc.read()
        if msg:
            ui.output(msg)
            
        msgout = ui.input()
        if msgout:
            if msgout.startswith("/quit"):
                sys.exit(0)
            
            
            else:
                if msgout.startswith("@"):
                    nickend = msgout.find(" ")
                    msgnick = msgout[0:nickend]
                    pmout = msgout[nickend + 1:-1]
                    irc.send("PRIVMSG :" + msgnick + " :" + pmout)
                    
                else:
                    irc.send("PRIVMSG " + CHAN + " :" + msgout)
                    ui.output("[" + NICK + "]: " + msgout)
        
        

curses.wrapper(main)
