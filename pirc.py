import socket
import re
import select
import errno

# Regexp to match PING messages
ping_re = re.compile("PING :(.*)")

# Regexp to match PRIVMSG messages
re_answer = re.compile(":(\w+)!.*PRIVMSG ([#\w]+) :(.*)")

# Regexp to match IRC lines in readbuffer
re_irc_lines = re.compile("(.*)\r\n")

# Regexp to remove IRC formatting
re_remove_formatting = re.compile(r"\x02|\x1f|\x16|\x0f")

# Regexp to remove color codes
re_remove_color = re.compile("\x03[0-9]{1,2}(,[0-9]{1,2})?")

class IRC(object):
    def __init__(self, host='irc.patwic.com', port = 6667, 
        server_password="",remove_formatting = True):
        self.host = host
        self.port = port
        self.server_password = server_password
        self.read_lines = []
        self.read_buffer = ''
        self.nick = ''
        self.remove_formatting = remove_formatting
        self.hostname = socket.getfqdn()
        self.sock = socket.socket()
        self.sock.connect((self.host, self.port))

    def connect(self, nick, realname):
        self.nick = nick

        if self.server_password:
            self.send('PASS {}'.format(self.server_password))

        self.send('NICK {}'.format(nick))
        self.send('USER {} {} bla :{}'.format(nick, self.hostname, realname))

    def send(self, msg):
        return self.sock.send(bytes(msg + '\r\n', 'utf-8'))

    def read(self, timeout=None):

        def strip_formatting(msg):
            msg = re_remove_formatting.sub("", msg)
            return re_remove_color.sub("", msg)

        self.sock.settimeout(timeout)

        if self.read_lines:
            return self.read_lines.pop(0)

        should_stop = False
        while not should_stop:
            try:
                i, o, e = select.select([self.sock], [], [], 0.0001)
                should_stop = True
            except OSError as oserror:
                if oserror.errno == errno.EINTR:
                    continue
                else:
                    raise

        for s in i:
            if s != self.sock:
                continue

            readbuffer = self.read_buffer + str(self.sock.recv(1024),'utf-8')

            line_iter = list(re_irc_lines.finditer(readbuffer))
            if line_iter:
                for l in line_iter:
                    # Handle PING messages
                    m = ping_re.search(l.group(1))
                    if m:
                        self.send("PONG :{}".format(m.group(1)))
                        continue
                    self.read_lines.append(l.group(1))

                self.read_buffer = readbuffer[line_iter[-1].end(1) + 2:]
            else:
                self.read_buffer = readbuffer

        if self.read_lines:
            line = self.read_lines.pop(0)
            if self.remove_formatting:
                return strip_formatting(line)
            else:
                return line
        else:
            return None
